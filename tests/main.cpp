﻿# ifdef _MSC_VER
#  define _SCL_SECURE_NO_WARNINGS
#  define _CRT_SECURE_NO_WARNINGS
# endif // _MSC_VER

#include <koicxx/crypt.hpp>
#include <koicxx/encoding.hpp>
#include <koicxx/filesystem.hpp>
#include <koicxx/floating_point.hpp>
#include <koicxx/http.hpp>
#include <koicxx/https.hpp>
#include <koicxx/macro_magic.hpp>
#include <koicxx/make_string.hpp>
#include <koicxx/property_tree.hpp>
#include <koicxx/string.hpp>
#include <koicxx/time.hpp>
#include <koicxx/to_underlying.hpp>
# ifdef _MSC_VER
#  include <koicxx/win.hpp>
# endif // _MSC_VER

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN

#include <boost/assert.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/date_time.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/test/unit_test_monitor.hpp>

# ifdef _MSC_VER
#  include <Windows.h>
#  include <Objbase.h>
# endif // _MSC_VER

#include <iostream>
#include <limits>
#include <sstream>
#include <string>
#include <vector>

void translate_boost_exception(const boost::exception& ex)
{
  BOOST_FAIL(boost::diagnostic_information(ex));
}

class Fixture
{
public:
  Fixture()
  {
    boost::unit_test::unit_test_monitor.register_exception_translator<boost::exception>(&translate_boost_exception);
  }
};

BOOST_GLOBAL_FIXTURE(Fixture)

BOOST_AUTO_TEST_SUITE(http_test_suite)

BOOST_AUTO_TEST_CASE(http_get_request_test_case)
{
  const koicxx::http::response server_response = koicxx::http::get_request("httpbin.org", "/get?foo=bar&baz=foobar");

  boost::property_tree::ptree pt;
  std::stringstream stream;
  stream << server_response.message_body;
  boost::property_tree::read_json(stream, pt);

  BOOST_CHECK_EQUAL(pt.get<std::string>("args.foo"), "bar");
  BOOST_CHECK_EQUAL(pt.get<std::string>("args.baz"), "foobar");
}

BOOST_AUTO_TEST_CASE(http_post_request_test_case)
{
  const koicxx::http::response server_response = koicxx::http::post_request("httpbin.org", "/post", "foo=bar&baz=foobar");

  boost::property_tree::ptree pt;
  std::stringstream stream;
  stream << server_response.message_body;
  boost::property_tree::read_json(stream, pt);

  BOOST_CHECK_EQUAL(pt.get<std::string>("form.foo"), "bar");
  BOOST_CHECK_EQUAL(pt.get<std::string>("form.baz"), "foobar");
}

BOOST_AUTO_TEST_SUITE_END() // http_test_suite

// =================================================

BOOST_AUTO_TEST_SUITE(https_test_suite)

BOOST_AUTO_TEST_CASE(https_get_request_test_case)
{
  const koicxx::https::response server_response = koicxx::https::get_request("httpbin.org", "/get?foo=bar&baz=foobar");

  boost::property_tree::ptree pt;
  std::stringstream stream;
  stream << server_response.message_body;
  boost::property_tree::read_json(stream, pt);

  BOOST_CHECK_EQUAL(pt.get<std::string>("args.foo"), "bar");
  BOOST_CHECK_EQUAL(pt.get<std::string>("args.baz"), "foobar");
}

BOOST_AUTO_TEST_CASE(https_post_request_test_case)
{
  const koicxx::https::response server_response = koicxx::https::post_request("httpbin.org", "/post", "foo=bar&baz=foobar");

  boost::property_tree::ptree pt;
  std::stringstream stream;
  stream << server_response.message_body;
  boost::property_tree::read_json(stream, pt);

  BOOST_CHECK_EQUAL(pt.get<std::string>("form.foo"), "bar");
  BOOST_CHECK_EQUAL(pt.get<std::string>("form.baz"), "foobar");
}

BOOST_AUTO_TEST_SUITE_END() // https_test_suite

// =================================================

#ifdef _MSC_VER

BOOST_AUTO_TEST_SUITE(win_test_suite)

BOOST_AUTO_TEST_CASE(win_get_error_message_from_boost_error_code_test_case)
{
  boost::system::error_code error_code;
  boost::filesystem::create_directory("$some_#invalid*characters@", error_code);
  if (error_code)
  {
    BOOST_CHECK_EQUAL(
      koicxx::win::get_error_message(error_code)
      , "The filename, directory name, or volume label syntax is incorrect."
    );
  }
}

BOOST_AUTO_TEST_CASE(win_get_error_message_from_hresult_test_case)
{
  HRESULT res = GetClassFile(L"", NULL);
  BOOST_CHECK_EQUAL(
    koicxx::win::get_error_message(res)
    , "The parameter is incorrect."
  );
}

BOOST_AUTO_TEST_CASE(win_get_last_error_message_test_case)
{
  BOOL res = CreateDirectoryA("$some_#invalid*characters@", NULL);
  if (!res)
  {
    BOOST_CHECK_EQUAL(
      koicxx::win::get_last_error_message()
      , "The filename, directory name, or volume label syntax is incorrect."
    );
  }
}

BOOST_AUTO_TEST_SUITE_END() // win_test_suite

#endif // _MSC_VER

// =================================================

BOOST_AUTO_TEST_SUITE(encoding_test_suite)

BOOST_AUTO_TEST_CASE(encoding_url_encode_test_case)
{
  BOOST_CHECK_EQUAL(
    koicxx::encoding::url_encode("some&symols!to*be%encoded")
    , "some%26symols%21to%2Abe%25encoded"
  );

#ifdef _MSC_VER

  BOOST_CHECK_EQUAL(
    koicxx::encoding::url_encode(L"some&symols!to*be%абв-encoded")
    , "some%26symols%21to%2Abe%25%D0%B0%D0%B1%D0%B2-encoded"
  );

#endif // _MSC_VER
}

BOOST_AUTO_TEST_CASE(encoding_html_encode_test_case)
{
  BOOST_CHECK_EQUAL(
    koicxx::encoding::html_encode(L"some<symbols>to&be\"encoded")
    , "some&lt;symbols&gt;to&amp;be&quot;encoded"
  );
}

BOOST_AUTO_TEST_SUITE_END() // encoding_test_suite

// =================================================

BOOST_AUTO_TEST_SUITE(float_test_suite)

BOOST_AUTO_TEST_CASE(float_round_test_case)
{
  double initial_value = 0.56789;
  double rounded_value = koicxx::floating_point::round(initial_value, 2);
  BOOST_CHECK(std::fabs(rounded_value - 0.57) <= std::numeric_limits<decltype(rounded_value)>::epsilon());
}

BOOST_AUTO_TEST_CASE(float_is_equal_test_case)
{
  BOOST_CHECK(koicxx::floating_point::is_equal(0.64, 0.8 * 0.8));
}

BOOST_AUTO_TEST_CASE(float_less_than_or_equal_test_case)
{
  BOOST_CHECK(koicxx::floating_point::less_than_or_equal(0.63, 0.8 * 0.8));
  BOOST_CHECK(koicxx::floating_point::less_than_or_equal(0.64, 0.8 * 0.8));
}

BOOST_AUTO_TEST_CASE(float_greater_than_or_equal_test_case)
{
  BOOST_CHECK(koicxx::floating_point::greater_than_or_equal(0.65, 0.8 * 0.8));
  BOOST_CHECK(koicxx::floating_point::greater_than_or_equal(0.64, 0.8 * 0.8));
}

BOOST_AUTO_TEST_SUITE_END() // float_test_suite

// =================================================

BOOST_AUTO_TEST_SUITE(property_tree_test_suite)

BOOST_AUTO_TEST_CASE(property_tree_construct_ini_pt_from_string_test_case)
{
  const boost::property_tree::ptree ini_pt = koicxx::property_tree::construct_pt_from_string(
    "[foo]\n"
    "bar=baz"
    , koicxx::property_tree::format::INI
  );
  BOOST_CHECK_EQUAL(ini_pt.get<std::string>("foo.bar"), "baz");
}

BOOST_AUTO_TEST_CASE(property_tree_construct_json_pt_from_string_test_case)
{
  const boost::property_tree::ptree json_pt = koicxx::property_tree::construct_pt_from_string(
    "{"
      "\"foo\": \"bar\""
    "}"
    , koicxx::property_tree::format::JSON
  );
  BOOST_CHECK_EQUAL(json_pt.get<std::string>("foo"), "bar");
}

BOOST_AUTO_TEST_CASE(property_tree_construct_xml_pt_from_string_test_case)
{
  const boost::property_tree::ptree xml_pt = koicxx::property_tree::construct_pt_from_string(
    "<foo>"
      "<bar>baz</bar>"
    "</foo>"
    , koicxx::property_tree::format::XML
  );
  BOOST_CHECK_EQUAL(xml_pt.get<std::string>("foo.bar"), "baz");
}

BOOST_AUTO_TEST_SUITE_END() // property_tree_test_suite

// =================================================

BOOST_AUTO_TEST_SUITE(macro_magic_test_suite)

BOOST_AUTO_TEST_CASE(macro_magic_array_size_test_case)
{
  const std::size_t arr_size = 5;
  int arr[arr_size];
  (void)arr; // Remove "unreferenced local variable" warning in MSVC
  BOOST_CHECK_EQUAL(KOICXX_ARRAY_SIZE(arr), arr_size);
}

BOOST_AUTO_TEST_CASE(macro_magic_copy_str_test_case)
{
  char dst[5];
  KOICXX_COPY_STR(dst, "str");
  BOOST_CHECK_EQUAL(std::string(dst), "str");

  KOICXX_COPY_STR(dst, "strstr");
  BOOST_CHECK_EQUAL(std::string(dst), "strs");
}

BOOST_AUTO_TEST_SUITE_END() // macro_magic_test_suite

// =================================================

BOOST_AUTO_TEST_SUITE(make_string_test_suite)

BOOST_AUTO_TEST_CASE(make_string_test_case)
{
  const std::string str = koicxx::make_string() << "str " << 1 << " str";
  BOOST_CHECK_EQUAL(str, "str 1 str");
}

BOOST_AUTO_TEST_CASE(make_wstring_test_case)
{
  const std::wstring str = koicxx::make_wstring() << L"str " << 1 << L" str";
  /**
   * BOOST_CHECK_EQUAL can't work with wide-strings
   * See https://svn.boost.org/trac/boost/ticket/1136
   * and http://boost.2283326.n4.nabble.com/BOOST-CHECK-EQUAL-and-std-wstring-td2560031.html for details
   */
  BOOST_CHECK(str == L"str 1 str");
}

BOOST_AUTO_TEST_SUITE_END() // make_string_test_suite

// =================================================

BOOST_AUTO_TEST_SUITE(string_test_suite)

BOOST_AUTO_TEST_CASE(string_safe_sprintf_test_case)
{
  std::unique_ptr<char[]> str = koicxx::string::safe_sprintf("%s %d %s", "str", 1, "str");
  BOOST_CHECK_EQUAL(str.get(), std::string("str 1 str"));
}

BOOST_AUTO_TEST_CASE(string_get_string_between_test_case)
{
  {
    const std::string& str = koicxx::string::get_string_between("str 1 str", "str", "str");
    BOOST_CHECK_EQUAL(str, " 1 ");
  }
  
  {
    const std::string& str = koicxx::string::get_string_between("some_text 1 some_text", "str", "str");
    BOOST_CHECK(str.empty());
  }
}

BOOST_AUTO_TEST_CASE(string_remove_spaces_test_case)
{
  std::string str = "  str	";
  koicxx::string::remove_spaces(str);
  BOOST_CHECK_EQUAL(str, "str");
}

BOOST_AUTO_TEST_CASE(string_remove_trailing_nulls_test_case)
{
  std::vector<char> some_vec = boost::assign::list_of('1')('2')('3')('4')('5')('\0')('\0');
  std::string str(some_vec.begin(), some_vec.end());
  koicxx::string::remove_trailing_nulls(str);
  BOOST_CHECK_EQUAL(str, "12345");
}

BOOST_AUTO_TEST_CASE(string_remove_trailing_cr_and_lf_characters_test_case)
{
  std::string str("str\r\n\r\n");
  koicxx::string::remove_trailing_cr_and_lf_characters(str);
  BOOST_CHECK_EQUAL(str, "str");
}

BOOST_AUTO_TEST_SUITE_END() // string_test_suite

// =================================================

BOOST_AUTO_TEST_SUITE(filesystem_test_suite)

BOOST_AUTO_TEST_CASE(filesystem_get_file_content_test_case)
{
  const boost::filesystem::path helper_file_path("helper.txt");
  boost::filesystem::ofstream helper_file(helper_file_path);
  BOOST_VERIFY(helper_file && "An error occurred while creating helper file");
  helper_file << "str 1 str";
  helper_file.close();

  const std::string& helper_file_content = koicxx::filesystem::get_file_content(helper_file_path);
  BOOST_CHECK_EQUAL(helper_file_content, "str 1 str");

  boost::system::error_code ec;
  boost::filesystem::remove(helper_file_path, ec);
  BOOST_ASSERT_MSG(!ec, "An error occurred while removing helper file");
}

BOOST_AUTO_TEST_SUITE_END() // filesystem_test_suite

// =================================================

BOOST_AUTO_TEST_SUITE(time_test_suite)

BOOST_AUTO_TEST_CASE(time_to_time_t_test_case)
{
  std::string ts("2002-01-20 23:59:59.000");
  boost::posix_time::ptime t(boost::posix_time::time_from_string(ts));
  std::time_t res = koicxx::time::to_time_t(t);
  BOOST_CHECK_EQUAL(res, 1011571199);
}

BOOST_AUTO_TEST_SUITE_END() // time_test_suite

// =================================================

# ifdef _MSC_VER

  BOOST_AUTO_TEST_SUITE(crypt_test_suite)

  BOOST_AUTO_TEST_CASE(crypt_get_hash_text_test_case)
  {
    char some_text[] = "some_text";
    const std::string hash_text(koicxx::crypt::get_hash_text(some_text, sizeof(some_text) / sizeof(*some_text) - 1, koicxx::crypt::hash_type::MD5));
    BOOST_CHECK_EQUAL(hash_text, "32d3f9b84bf99ae5faecc315d389c894");
  }

  BOOST_AUTO_TEST_SUITE_END() // crypt_test_suite

# endif // _MSC_VER

// =================================================

BOOST_AUTO_TEST_SUITE(to_underlying_test_suite)

BOOST_AUTO_TEST_CASE(to_underlying_test_case)
{
  enum class Foo { BAR, BAZ };
  const Foo first = Foo::BAR;
  const Foo second = Foo::BAZ;
  BOOST_CHECK_EQUAL(koicxx::to_underlying(first), 0);
  BOOST_CHECK_EQUAL(koicxx::to_underlying(second), 1);
}

BOOST_AUTO_TEST_SUITE_END() // to_underlying_test_suite
