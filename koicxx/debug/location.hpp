#ifndef KOICXX_DEBUG_LOCATION_HPP
#define KOICXX_DEBUG_LOCATION_HPP

#include <koicxx/filesystem.hpp>

#include <string>

namespace koicxx {
namespace debug {

class location
{
public:
  location()
    : _file_name("Unknown")
    , _func_name("Unknown")
    , _line(-1) {}

  location(
    const std::string& file_name
    , const std::string& func_name
    , int line
  )
    : _file_name(koicxx::filesystem::get_file_name_without_path(file_name))
    , _func_name(func_name)
    , _line(line) {}

  std::string file_name() const
  {
    return _file_name;
  }

  std::string func_name() const
  {
    return _func_name;
  }

  int line() const
  {
    return _line;
  }

private:
  const std::string _file_name;
  const std::string _func_name;
  const int _line;
};

inline
std::ostream& operator<<(std::ostream& os, const location& l)
{
  os << l.func_name() << " (" << l.file_name() << ':' << l.line() << ')';
  return os;
}

} // namespace debug
} // namespace koicxx

#endif // !KOICXX_DEBUG_LOCATION_HPP