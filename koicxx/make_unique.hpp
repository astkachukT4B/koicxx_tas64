#ifndef KOICXX_MAKE_UNIQUE_HPP
#define KOICXX_MAKE_UNIQUE_HPP

#include <boost/config.hpp>

#include <ctime>
#include <memory>
#include <type_traits>

namespace koicxx {

#if !defined(BOOST_NO_STATIC_ASSERT) && !defined(BOOST_NO_VARIADIC_TEMPLATES)

namespace detail {

template <typename T, typename... Args>
std::unique_ptr<T> make_unique_helper(std::false_type, Args&&... args)
{
  return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

template <typename T, typename... Args>
std::unique_ptr<T> make_unique_helper(std::true_type, Args&&... args)
{
  static_assert(std::extent<T>::value == 0, "make_unique<T[N]() should be avoided, use make_unique<T[]> instead");

  typedef typename std::remove_extent<T>::type U;
  return std::unique_ptr<T>(new U[sizeof...(Args)]{std::forward<Args>(args)...});
}

} // namespace detail

/**
 * \brief Creates a std::unique_ptr object in a std::make_shared manner
 */
template <typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args)
{
  return detail::make_unique_helper<T>(std::is_array<T>(), std::forward<Args>(args)...);
}

#endif // !BOOST_NO_STATIC_ASSERT && !BOOST_NO_VARIADIC_TEMPLATES

} // namespace koicxx

#endif // !KOICXX_MAKE_UNIQUE_HPP