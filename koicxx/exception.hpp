#ifndef KOICXX_EXCEPTION_HPP
#define KOICXX_EXCEPTION_HPP

#include <koicxx/debug/location.hpp>
#include <koicxx/debug/stack_trace.hpp>

#include <boost/assert.hpp>
#include <boost/config.hpp>
#include <boost/current_function.hpp>
#include <boost/exception/all.hpp>

#include <exception>
#include <string>
#include <vector>

# ifdef BOOST_NO_RTTI

#   define KOICXX_THROW(Ex) \
      throw Ex << koicxx::exception::location_info(koicxx::debug::location(__FILE__, BOOST_CURRENT_FUNCTION, __LINE__)) \
               << koicxx::exception::original_type_info("Unknown") \
               << koicxx::exception::trace_info(koicxx::debug::get_call_stack())

# else // !BOOST_NO_RTTI

#include <typeinfo>

#   define KOICXX_THROW(Ex) \
      throw Ex << koicxx::exception::location_info(koicxx::debug::location(__FILE__, BOOST_CURRENT_FUNCTION, __LINE__)) \
               << koicxx::exception::original_type_info(typeid((Ex)).name()) \
               << koicxx::exception::trace_info(koicxx::debug::get_call_stack())

# endif

namespace koicxx {

class exception
  : virtual public std::exception
  , virtual public boost::exception
{
public:
  typedef boost::error_info<struct tag_backtrace, std::vector<std::string>>       trace_info;
  typedef boost::error_info<struct tag_location, debug::location>                 location_info;
  typedef boost::error_info<struct tag_reason, std::string>                       reason_info;
  typedef boost::error_info<struct tag_original_type_info, const char* const>     original_type_info;
  typedef boost::error_info<struct tag_nested_exception_info, std::exception_ptr> nested_exception_info;

  /**
   * \brief Helper function to attach exception reason
   */
  static reason_info reason(const std::string& str)
  {
    return reason_info(str);
  }

  /**
   * \brief Helper function to attach current exception
   */
  static nested_exception_info current_exception()
  {
    BOOST_ASSERT_MSG(std::current_exception() != nullptr, "This function must be called in catch() block only!");
    return nested_exception_info(std::current_exception());
  }

  /**
   * \brief Helper function for getting exception info
   */
  template <typename T>
  const typename T::value_type* get() const
  {
    return boost::get_error_info<T>(*this);
  }

  /**
   * \return Call stack where exception occurred
   */
  std::vector<std::string> call_stack() const
  {
    if (const std::vector<std::string>* result = get<trace_info>())
    {
      return *result;
    }
    return std::vector<std::string>();
  }

  /**
   * \return Information about exception reason
   */
  virtual std::string reason() const
  {
    if (const std::string* result = get<reason_info>())
    {
      return *result;
    }
    return std::string();
  }

  /**
   * \return Information about original exception type
   */
  std::string origin_type_info() const
  {
    if (const char* const* result = get<original_type_info>())
    {
      return *result;
    }
    return std::string();
  }

  /**
   * \return Information about location where exception occurred
   */
  debug::location where() const
  {
    if (const debug::location* result = get<location_info>())
    {
      return *result;
    }
    return debug::location();
  }

  /**
   * \return \c true if some other exception has attached to this one
   */
  bool has_nested_exception() const
  {
    return get<nested_exception_info>() != nullptr;
  }

  /**
   * \brief Throws a nested exception if attached, otherwise calls \c std::terminate function
   */
  void rethrow_nested_exception() const
  {
    if (auto* ptr = get<nested_exception_info>())
    {
      std::rethrow_exception(*ptr);
    }
    std::terminate();
  }

  /**
   * \return All information about exception that could be retreived via \c boost::diagnostic_information_what function call
   */
  /* virtual */ const char* what() const BOOST_NOEXCEPT override
  {
    return boost::diagnostic_information_what(*this);
  }
};

} // namespace koicxx

#endif // !KOICXX_EXCEPTION_HPP
