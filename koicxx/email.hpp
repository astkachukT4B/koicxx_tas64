#ifndef KOICXX_EMAIL_HPP
#define KOICXX_EMAIL_HPP

# ifdef _MSC_VER
#  define NOMINMAX
# endif

#include <koicxx/exception.hpp>
#include <koicxx/make_string.hpp>

#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/date_time.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>

#include <iterator>
#include <string>
#include <vector>

namespace koicxx {
namespace email {

class exception : public koicxx::exception {};

namespace detail {

typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;

const int SERVICE_READY     = 220;
const int MAIL_ACTION_OK    = 250;
const int AUTH_SUCCESS      = 235;
const int AUTH_IN_PROCESS   = 334;
const int START_MAIL_INPUT  = 354;

template <typename SOCKET_TYPE>
void send_command(SOCKET_TYPE& socket, const std::string& data)
{
  boost::asio::streambuf request;
  std::ostream request_stream(&request);
  request_stream << data << "\r\n";
  boost::asio::write(socket, request);
}

struct smtp_server_response
{
  smtp_server_response() : ret_code(-1) {}

  int ret_code;
  std::string last_line_text;
};

template <typename SOCKET_TYPE>
smtp_server_response get_server_response(SOCKET_TYPE& socket)
{
  smtp_server_response res;

  while (true)
  {
    /**
     * Trying to get first chunk of the server response.
     * According to the boost documentation (http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference/read_until/overload3.html),
     * "After a successful read_until operation, the streambuf may contain additional data beyond the delimiter"
     */
    boost::asio::streambuf response;
    boost::asio::read_until(socket, response, "\r\n");
    const std::string response_str = koicxx::make_string() << &response;

    /**
     * Trying to get last response line
     * According to the SMTP RFC 5321 (https://tools.ietf.org/html/rfc5321),
     * "The format for multiline replies requires that every line, except the
     * last, begin with the reply code, followed immediately by a hyphen,
     * "-" (also known as minus), followed by text.  The last line will
     * begin with the reply code, followed immediately by <SP>, optionally
     * some text, and <CRLF>.
     * [...]
     * In a multiline reply, the reply code on each of the lines MUST be the
     * same"
     */
    boost::smatch results;
    if (boost::regex_search(response_str.begin(), response_str.end(), results, boost::regex("^(\\d+) (.*)$")))
    {
      res.ret_code = std::stoi(results[1]);
      res.last_line_text = results[2];
    }
    else
    {
      // There's another chunk of the server response, so let's wait for it
      continue;
    }

    break;
  }

  return res;
}

typedef boost::archive::iterators::base64_from_binary<boost::archive::iterators::transform_width<const char *, 6, 8>> base64_text;

std::string encode_base64(const std::string& data)
{
  std::stringstream os;
  std::size_t sz = data.size();
  std::copy(base64_text(data.c_str()), base64_text(data.c_str() + sz), std::ostream_iterator<char>(os));
  return os.str();
}

std::string get_current_date_time(const char* fmt)
{
  const boost::local_time::time_zone_ptr utc_time_zone(new boost::local_time::posix_time_zone("GMT"));
  boost::local_time::local_time_facet* facet = new boost::local_time::local_time_facet(fmt);

  const boost::posix_time::ptime& my_ptime = boost::posix_time::second_clock::universal_time();
  boost::local_time::local_date_time now(my_ptime, utc_time_zone);

  std::ostringstream date_osstr;
  date_osstr.imbue(std::locale(date_osstr.getloc(), facet));
  date_osstr << now;

  return date_osstr.str();
}

template <typename SOCKET_TYPE>
void wait_for_service_ready(SOCKET_TYPE& socket)
{
  /**
   * According to the SMTP RFC 5321 (https://tools.ietf.org/html/rfc5321),
   * "Normally, a receiver will send a 220 "Service ready" reply when the connection is
   * completed. The sender SHOULD wait for this greeting message before
   * sending any commands"
   */
  smtp_server_response server_response = get_server_response(socket);
  if (server_response.ret_code != SERVICE_READY)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while waiting for \"Service Ready\" message from the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }
}

template <typename SOCKET_TYPE>
void send_ehlo(SOCKET_TYPE& socket, const std::string& smtp_server_addr)
{
  send_command(socket, "EHLO " + smtp_server_addr);
  smtp_server_response server_response = get_server_response(socket);
  if (server_response.ret_code != MAIL_ACTION_OK)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending \"EHLO\" command to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }
}

template <typename SOCKET_TYPE>
void auth_login(SOCKET_TYPE& socket, const std::string& email, const std::string& password)
{
  send_command(socket, "AUTH LOGIN");
  smtp_server_response server_response = get_server_response(socket);
  if (server_response.ret_code != AUTH_IN_PROCESS)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending \"AUTH LOGIN\" command to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }

  send_command(socket, encode_base64(email));
  server_response = get_server_response(socket);
  if (server_response.ret_code != AUTH_IN_PROCESS)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending message contains username to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }

  send_command(socket, encode_base64(password));
  server_response = get_server_response(socket);
  if (server_response.ret_code != AUTH_SUCCESS)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending message contains password to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }
}

template <typename SOCKET_TYPE>
void send_mail_from(SOCKET_TYPE& socket, const std::string& from_email)
{
  send_command(socket, "MAIL FROM:<" + from_email + ">");
  smtp_server_response server_response = get_server_response(socket);
  if (server_response.ret_code != MAIL_ACTION_OK)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending \"MAIL\" command to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }
}

template <typename SOCKET_TYPE>
void send_rcpt_to(SOCKET_TYPE& socket, const std::string& to_email)
{
  send_command(socket, "RCPT TO:<" + to_email + ">");
  smtp_server_response server_response = get_server_response(socket);
  if (server_response.ret_code != MAIL_ACTION_OK)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending \"RCPT\" command to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }
}

template <typename SOCKET_TYPE>
void send_data(SOCKET_TYPE& socket, const std::string& from_email, const std::string& to_email, const std::string& subject, const std::string& body)
{
  send_command(socket, "DATA");
  /**
   * According to the SMTP RFC 5321 (https://tools.ietf.org/html/rfc5321),
   * "message data MUST NOT be sent unless a 354 reply is received"
  */
  smtp_server_response server_response = get_server_response(socket);
  if (server_response.ret_code != START_MAIL_INPUT)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending \"DATA\" message to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }

  send_command(socket, "From: <" + from_email + ">");
  send_command(socket, "To: " + to_email);
  send_command(socket, "Subject: " + subject);
  send_command(socket, "Date: " + get_current_date_time("%d %b %Y %T %q")); // Example: "29 Apr 2014 06:47:38 +0000"
  send_command(socket, "");
  send_command(socket, body);
  send_command(socket, ".");
  server_response = get_server_response(socket);
  if (server_response.ret_code != MAIL_ACTION_OK)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending message contains data to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }
}

} // namespace detail

enum class crypt_protocol { NONE, SSL };

void send_email(
  const std::string& smtp_server_addr
  , const int smtp_server_port
  , const std::string& from
  , const std::string& password
  , const std::string& to
  , const std::string& subject
  , const std::string& body
  , const crypt_protocol crypt_proto
)
{
  try
  {
    if (crypt_proto == crypt_protocol::NONE)
    {
      boost::asio::io_service io_service;
      boost::asio::ip::tcp::resolver resolver(io_service);
      boost::asio::ip::tcp::resolver::query query(smtp_server_addr, boost::lexical_cast<std::string>(smtp_server_port));

      boost::asio::ip::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
      boost::asio::ip::tcp::socket socket(io_service);
      boost::asio::connect(socket, endpoint_iterator);

      detail::wait_for_service_ready(socket);
      detail::send_ehlo(socket, smtp_server_addr);
      detail::auth_login(socket, from, password);
      detail::send_mail_from(socket, from);
      detail::send_rcpt_to(socket, to);
      detail::send_data(socket, from, to, subject, body);
    }
    else if (crypt_proto == crypt_protocol::SSL)
    {
      boost::asio::ssl::context ctx(boost::asio::ssl::context::sslv23);
      ctx.set_default_verify_paths();

      boost::asio::io_service io_service;
      detail::ssl_socket socket(io_service, ctx);
      boost::asio::ip::tcp::resolver resolver(io_service);
      boost::asio::ip::tcp::resolver::query query(smtp_server_addr, boost::lexical_cast<std::string>(smtp_server_port));

      boost::asio::connect(socket.lowest_layer(), resolver.resolve(query));
      socket.lowest_layer().set_option(boost::asio::ip::tcp::no_delay(true));

      socket.set_verify_mode(boost::asio::ssl::context::verify_none);
      socket.handshake(detail::ssl_socket::client);

      detail::wait_for_service_ready(socket);
      detail::send_ehlo(socket, smtp_server_addr);
      detail::auth_login(socket, from, password);
      detail::send_mail_from(socket, from);
      detail::send_rcpt_to(socket, to);
      detail::send_data(socket, from, to, subject, body);
    }
    else
    {
      KOICXX_THROW(exception())
        << koicxx::exception::reason(
          "Unsupported crypt. protocol"
      );
    }
  }
  catch (const exception& ex)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        ex.reason()
    );
  }
  catch (const std::exception& ex)
  {
  	KOICXX_THROW(exception())
      << koicxx::exception::reason(
        ex.what()
    );
  }
}

} // namespace email
} // namespace koicxx

#endif // !KOICXX_EMAIL_HPP