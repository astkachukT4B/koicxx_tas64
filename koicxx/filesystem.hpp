#ifndef KOICXX_FILESYSTEM_HPP
#define KOICXX_FILESYSTEM_HPP

#include <koicxx/macro_magic.hpp>

#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

# ifdef _MSC_VER
#  define WIN32_LEAN_AND_MEAN
#  include <Windows.h>
# endif // _MSC_VER

#include <fstream>
#include <iterator>
#include <string>

namespace koicxx {
namespace filesystem {

inline
std::string get_file_content(const boost::filesystem::path& file_path)
{
  std::string file_content;

  boost::filesystem::ifstream f(file_path);
  if (f)
  {
    file_content.append(
      (std::istreambuf_iterator<char>(f))
      , std::istreambuf_iterator<char>()
    );
  }

  return file_content;
}

enum class file_encoding
{
  UTF_8
};

/**
 * \brief Removes BOM from UTF8-encoded file
 * \return true on success, false otherwise
 *
 * \todo Add non-UTF8 files support
 */
inline
bool remove_bom_from_file(const boost::filesystem::path& path, file_encoding encoding)
{
  std::string all_data_from_templates_file = get_file_content(path);
  if (all_data_from_templates_file.empty())
  {
    return false;
  }

  if (encoding == file_encoding::UTF_8)
  {
    try
    {
      // Remove "truncation of constant value" warning in MSVC
      # ifdef _MSC_VER
      #  pragma warning(push)
      #  pragma warning(disable:4309)
      # endif // _MSC_VER
        
      if (all_data_from_templates_file.at(0) == static_cast<char>(0xEF) 
        && all_data_from_templates_file.at(1) == static_cast<char>(0xBB)
        && all_data_from_templates_file.at(2) == static_cast<char>(0xBF)
      )
      {
        all_data_from_templates_file.erase(0, 3);
      }
      else
      {
        return true;
      }

      # ifdef _MSC_VER
      #  pragma warning(pop)
      # endif // _MSC_VER
    }
    catch (const std::out_of_range&)
    {
      // Just ignore it (maybe empty file)
      return true;
    }
  }
  else
  {
    // Unsupported file encoding
    return false;
  }

  boost::filesystem::ofstream templates_file_out(path);
  if (!templates_file_out)
  {
    return false;
  }
  templates_file_out << all_data_from_templates_file;

  return true;
}

/**
 * \brief Returns filename without path
 */
inline
std::string get_file_name_without_path(const std::string& full_path)
{
  return boost::filesystem::path(full_path).filename().string();
}

#ifdef _MSC_VER

/**
 * \brief Returns path of the current exe
 * \return Empty boost::filesystem::path object on failure
 */
inline
boost::filesystem::path get_cur_exe_path()
{
  try
  {
    char cpath[MAX_PATH];
    const DWORD size = KOICXX_ARRAY_SIZE(cpath);
    const DWORD res = GetModuleFileNameA(NULL, cpath, size);
    if (res == size || !res)
    {
      return boost::filesystem::path();
    }

    const boost::filesystem::path path(cpath);
    return path.parent_path();
  }
  catch (const boost::filesystem::filesystem_error&)
  {
    return boost::filesystem::path();
  }
}

#endif // _MSC_VER

} // namespace filesystem
} // namespace koicxx

#endif // !KOICXX_FILESYSTEM_HPP