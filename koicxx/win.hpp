#ifndef KOICXX_WIN_HPP
#define KOICXX_WIN_HPP

# ifndef _MSC_VER
#  error This is MSVC-specific header file
# endif // !_MSC_VER

#define _SCL_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS

#define KOICXX_MAGIC_SIZE 256 // Feel the magic!

#include <koicxx/exception.hpp>
#include <koicxx/make_string.hpp>
#include <koicxx/string.hpp>

#include <boost/assert.hpp>
#include <boost/scope_exit.hpp>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <atlbase.h>
#include <Commctrl.h>
#include <Mshtml.h>
#include <Objbase.h>
#include <Oleacc.h>
#include <TlHelp32.h>

#include <sstream>
#include <string>
#include <vector>

namespace koicxx {
namespace win {

class exception : public koicxx::exception {};

/**
 * \brief Gets the error message in english from the specified error code
 * \return Error message in english on success, "Unknown" otherwise
 */
inline
std::string get_error_message(const boost::system::error_code& err_code)
{
  LPSTR error_text = NULL;
  if (!FormatMessageA(
    FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_IGNORE_INSERTS,
    NULL,
    err_code.value(),
    MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
    (LPSTR)&error_text,
    0,
    NULL
  ))
  {
    return "Unknown";
  }
  BOOST_SCOPE_EXIT_ALL(error_text)
  {
    BOOST_VERIFY(
      LocalFree(error_text) == NULL
      && "An error occurred while using function LocalFree"
    );
  };

  std::string res(error_text);
  koicxx::string::remove_trailing_cr_and_lf_characters(res);
  return res;
}

/**
 * \brief Gets the error message in english from the specified error code
 * \return Error message in english on success, "Unknown" otherwise
 */
inline
std::string get_error_message(HRESULT error_code)
{
  LPSTR error_text = NULL;
  if (!FormatMessageA(
    FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_IGNORE_INSERTS,
    NULL,
    error_code,
    MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
    (LPSTR)&error_text,
    0,
    NULL
  ))
  {
    return "Unknown";
  }
  BOOST_SCOPE_EXIT_ALL(error_text)
  {
    BOOST_VERIFY(
      LocalFree(error_text) == NULL
      && "An error occurred while using function LocalFree"
    );
  };

  std::string res(error_text);
  koicxx::string::remove_trailing_cr_and_lf_characters(res);
  return res;
}

/**
 * \brief Gets the last error message in english
 * \return Error message in english on success, "Unknown" otherwise
 */
inline
std::string get_last_error_message()
{
  DWORD last_error_code = GetLastError();
  LPSTR error_text = NULL;
  if (!FormatMessageA(
    FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_IGNORE_INSERTS,
    NULL,
    last_error_code,
    MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
    (LPSTR)&error_text,
    0,
    NULL
  ))
  {
    return "Unknown";
  }
  BOOST_SCOPE_EXIT_ALL(error_text)
  {
    BOOST_VERIFY(
      LocalFree(error_text) == NULL
      && "An error occurred while using function LocalFree"
    );
  };

  std::string res(error_text);
  koicxx::string::remove_trailing_cr_and_lf_characters(res);
  return res;
}

#ifdef UNICODE

/**
 * \brief Gets the processes IDs for the processes with the specified name
 * \throw koicxx::win::exception on failure
 */
inline
std::vector<DWORD> get_processes_ids(const std::wstring& process_name)
{
  std::vector<DWORD> processes_ids;

  HANDLE process_snapshot_handle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  if (process_snapshot_handle == INVALID_HANDLE_VALUE)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function CreateToolhelp32Snapshot. Error code: " << GetLastError());
  }
  BOOST_SCOPE_EXIT_ALL(process_snapshot_handle)
  {
    BOOST_VERIFY(
      CloseHandle(process_snapshot_handle)
      && "An error occurred while using function CloseHandle"
    );
  };

  PROCESSENTRY32W pe32;
  pe32.dwSize = sizeof(PROCESSENTRY32W);

  if (Process32First(process_snapshot_handle, &pe32) == FALSE)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function Process32First. Error code: " << GetLastError());
  }

  do
  {
    const std::wstring process_exe_file = pe32.szExeFile;
    if (process_exe_file == process_name)
    {
      processes_ids.push_back(pe32.th32ProcessID);
    }
  } while (Process32Next(process_snapshot_handle, &pe32) == TRUE);

  return processes_ids;
}

#else // !UNICODE

/**
 * \brief Gets the processes IDs for the processes with the specified name
 * \throw koicxx::win::exception on failure
 */
inline
std::vector<DWORD> get_processes_ids(const std::string& process_name)
{
  std::vector<DWORD> processes_ids;

  HANDLE process_snapshot_handle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  if (process_snapshot_handle == INVALID_HANDLE_VALUE)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function CreateToolhelp32Snapshot. Error code: " << GetLastError());
  }
  BOOST_SCOPE_EXIT_ALL(process_snapshot_handle)
  {
    BOOST_VERIFY(
      CloseHandle(process_snapshot_handle)
      && "An error occurred while using function CloseHandle"
    );
  };

  PROCESSENTRY32 pe32;
  pe32.dwSize = sizeof(PROCESSENTRY32);

  if (Process32First(process_snapshot_handle, &pe32) == FALSE)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function Process32First. Error code: " << GetLastError());
  }

  do
  {
    const std::string process_exe_file = pe32.szExeFile;
    if (process_exe_file == process_name)
    {
      processes_ids.push_back(pe32.th32ProcessID);
    }
  } while (Process32Next(process_snapshot_handle, &pe32) == TRUE);

  return processes_ids;
}

#endif

/**
 * \brief Finds the window with the specified process ID and styles
 */
class find_window
{
public:
  find_window(
      DWORD process_id
      , std::vector<LONG> styles = std::vector<LONG>()
    )
    : _process_id(process_id)
    , _styles(styles)
    , _res(NULL) {}

  HWND operator()()
  {
    EnumWindows(
      [](HWND cur_window_hwnd, LPARAM this_ptr) -> BOOL
      {
        find_window* instance = reinterpret_cast<find_window*>(this_ptr);
        return instance->enum_windows_callback_impl(cur_window_hwnd);
      }
      , reinterpret_cast<LPARAM>(this)
    );

    return _res;
  }

private:
  BOOL CALLBACK enum_windows_callback_impl(HWND cur_window_hwnd)
  {
    const LONG cur_window_style = GetWindowLongA(cur_window_hwnd, GWL_STYLE);
    if (!cur_window_style)
    {
      return TRUE;
    }

    for (const auto& style : _styles)
    {
      if (!(cur_window_style & style))
      {
        return TRUE;
      }
    }

    DWORD cur_window_process_id;
    GetWindowThreadProcessId(cur_window_hwnd, &cur_window_process_id);
    if (cur_window_process_id == _process_id)
    {
      _res = cur_window_hwnd;
      return FALSE;
    }

    return TRUE;
  }

  const DWORD _process_id;
  const std::vector<LONG> _styles;
  HWND _res;
};

/**
 * \brief Finds the child window for the specified parent window with the specified window name and styles
 */
class find_child_window
{
public:
  find_child_window(
      HWND parent_hwnd
      , const std::string& window_name
      , std::vector<LONG> styles = std::vector<LONG>()
    )
    : _parent_hwnd(parent_hwnd)
    , _window_name(window_name)
    , _styles(styles)
    , _res(NULL) {}

  HWND operator()()
  {
    EnumChildWindows(
      _parent_hwnd
      , [](HWND cur_window_hwnd, LPARAM this_ptr) -> BOOL
      {
        find_child_window* instance = reinterpret_cast<find_child_window*>(this_ptr);
        return instance->enum_child_windows_callback_impl(cur_window_hwnd);
      }
      , reinterpret_cast<LPARAM>(this)
    );

    return _res;
  }

private:
  BOOL CALLBACK enum_child_windows_callback_impl(HWND cur_child_window_hwnd)
  {
    const LONG cur_child_window_style = GetWindowLongA(cur_child_window_hwnd, GWL_STYLE);
    if (!cur_child_window_style)
    {
      return TRUE;
    }

    for (const auto& style : _styles)
    {
      if (!(cur_child_window_style & style))
      {
        return TRUE;
      }
    }

    char cur_child_window_name[KOICXX_MAGIC_SIZE];
    GetClassNameA(cur_child_window_hwnd, cur_child_window_name, sizeof(cur_child_window_name));
    if (cur_child_window_name == _window_name)
    {
      _res = cur_child_window_hwnd;
      return FALSE;
    }

    return TRUE;
  }

  const HWND _parent_hwnd;
  const std::string _window_name;
  const std::vector<LONG> _styles;
  HWND _res;
};

/**
 * \brief Gets the list view header for the specified list view
 */
inline
HWND get_list_view_header_hwnd(HWND list_view_hwnd)
{
  return (HWND)SendMessageA(
    list_view_hwnd
    , LVM_GETHEADER
    , NULL
    , NULL
  );
}

/**
 * \brief Gets the list view header element for the specified process and list view
 * \throw koicxx::win::exception on failure
 */
inline
std::string get_list_view_header_element(HANDLE process_handle, HWND list_view_header_hwnd, int column_index)
{
  HDITEMA* _hdi = (HDITEMA*)VirtualAllocEx(
    process_handle
    , NULL
    , sizeof(HDITEMA)
    , MEM_COMMIT
    , PAGE_READWRITE
  );
  if (_hdi == NULL)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while allocating memory for _hdi via VirtualAllocEx function. Error code: " << GetLastError());
  }
  BOOST_SCOPE_EXIT_ALL(_hdi, process_handle)
  {
    BOOST_VERIFY(VirtualFreeEx(
        process_handle
        , _hdi
        , 0
        , MEM_RELEASE
      ) && "An error occurred while using function VirtualFreeEx for _hdi"
    );
  };

  char* _column_header = (char*)VirtualAllocEx(
    process_handle
    , NULL
    , KOICXX_MAGIC_SIZE
    , MEM_COMMIT
    , PAGE_READWRITE
  );
  if (_column_header == NULL)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while allocating memory for _column_header via VirtualAllocEx function. Error code: " << GetLastError());
  }
  BOOST_SCOPE_EXIT_ALL(_column_header, process_handle)
  {
    BOOST_VERIFY(VirtualFreeEx(
        process_handle
        , _column_header
        , 0
        , MEM_RELEASE
      ) && "An error occurred while using function VirtualFreeEx for _column_header"
    );
  };

  HDITEMA hdi = { 0 };
  hdi.mask = HDI_TEXT;
  hdi.pszText = _column_header;
  hdi.cchTextMax = KOICXX_MAGIC_SIZE;

  if (!WriteProcessMemory(
    process_handle
    , _hdi
    , &hdi
    , sizeof(HDITEMA)
    , NULL
  ))
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function WriteProcessMemory. Error code: " << GetLastError());
  }
  if (SendMessageA(
    list_view_header_hwnd
    , HDM_GETITEM
    , (WPARAM)column_index
    , (LPARAM)_hdi
  ) == FALSE)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason("An error occurred while using function SendMessageA");
  }

  char column_header[KOICXX_MAGIC_SIZE];
  if (!ReadProcessMemory(
    process_handle
    , _column_header
    , column_header
    , KOICXX_MAGIC_SIZE
    , NULL
  ))
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function ReadProcessMemory. Error code: " << GetLastError());
  }

  return column_header;
}

/**
 * \brief Gets the list view header elements count for the specified list view
 */
inline
int get_list_view_elements_count(HWND list_view_hwnd)
{
  return (int)SendMessageA(
    list_view_hwnd
    , LVM_GETITEMCOUNT
    , NULL
    , NULL
  );
}

/**
 * \brief Gets the list view element for the specified process and list view
 * \throw koicxx::win::exception on failure
 */
inline
std::string get_list_view_element(HANDLE process_handle, HWND list_view_hwnd, int item_index, int subitem_index)
{
  LVITEMA* _lvi = (LVITEMA*)VirtualAllocEx(
    process_handle
    , NULL
    , sizeof(LVITEMA)
    , MEM_COMMIT
    , PAGE_READWRITE
  );
  if (_lvi == NULL)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while allocating memory for _lvi via VirtualAllocEx function. Error code: " << GetLastError());
  }
  BOOST_SCOPE_EXIT_ALL(_lvi, process_handle)
  {
    BOOST_VERIFY(VirtualFreeEx(
        process_handle
        , _lvi
        , 0
        , MEM_RELEASE
      ) && "An error occurred while using function VirtualFreeEx for _lvi"
    );
  };

  char* _subitem = (char*)VirtualAllocEx(
    process_handle
    , NULL
    , KOICXX_MAGIC_SIZE
    , MEM_COMMIT
    , PAGE_READWRITE
  );
  if (_subitem == NULL)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while allocating memory for _subitem via VirtualAllocEx function. Error code: " << GetLastError());
  }
  BOOST_SCOPE_EXIT_ALL(_subitem, process_handle)
  {
    BOOST_VERIFY(VirtualFreeEx(
        process_handle
        , _subitem
        , 0
        , MEM_RELEASE
      ) && "An error occurred while using function VirtualFreeEx for _subitem"
    );
  };

  LVITEMA lvi;
  lvi.cchTextMax = KOICXX_MAGIC_SIZE;
  lvi.iSubItem = subitem_index;
  lvi.pszText = _subitem;

  if (!WriteProcessMemory(
    process_handle
    , _lvi
    , &lvi
    , sizeof(LVITEMA)
    , NULL
  ))
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function WriteProcessMemory. Error code: " << GetLastError());
  }
  SendMessageA(
    list_view_hwnd
    , LVM_GETITEMTEXTA
    , (WPARAM)item_index
    , (LPARAM)_lvi
    );

  char subitem[KOICXX_MAGIC_SIZE];
  if (!ReadProcessMemory(
    process_handle
    , _subitem
    , subitem
    , KOICXX_MAGIC_SIZE
    , NULL
  ))
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function ReadProcessMemory. Error code: " << GetLastError());
  }

  return subitem;
}

/**
 * \brief Double clicks the list view element for the specified process and list view
 * \throw koicxx::win::exception on failure
 */
inline
void double_click_list_view_element(HANDLE process_handle, HWND list_view_hwnd, int element_index)
{
  LVITEMA* _lvi_for_select_item = (LVITEMA*)VirtualAllocEx(
    process_handle
    , NULL
    , sizeof(LVITEMA)
    , MEM_COMMIT
    , PAGE_READWRITE
  );
  if (_lvi_for_select_item == NULL)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while allocating memory for _lvi_for_select_item via VirtualAllocEx function. Error code: " << GetLastError());
  }
  BOOST_SCOPE_EXIT_ALL(_lvi_for_select_item, process_handle)
  {
    BOOST_VERIFY(VirtualFreeEx(
        process_handle
        , _lvi_for_select_item
        , 0
        , MEM_RELEASE
      ) && "An error occurred while using function VirtualFreeEx for _lvi_for_select_item"
    );
  };

  LVITEMA lvi_for_select_item;
  lvi_for_select_item.stateMask = 0x000F;
  lvi_for_select_item.state = LVIS_FOCUSED | LVIS_SELECTED;

  if (!WriteProcessMemory(
    process_handle
    , _lvi_for_select_item
    , &lvi_for_select_item
    , sizeof(LVITEMA)
    , NULL
  ))
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function WriteProcessMemory. Error code: " << GetLastError());
  }

  if (SendMessageA(
    list_view_hwnd
    , LVM_SETITEMSTATE
    , (WPARAM)element_index
    , (LPARAM)_lvi_for_select_item
  ) == FALSE)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason("An error occurred while using function SendMessageA to select item");
  }

  if (SendMessageA(
    list_view_hwnd
    , WM_LBUTTONDBLCLK
    , NULL
    , NULL
  ))
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason("An error occurred while using function SendMessageA to double click");
  }
}

/**
 * \brief Gets an HTML content for the specified internet explorer server window
 * \throw koicxx::win::exception on failure
 */
inline
std::wstring get_html_content(HWND internet_explorer_server_hwnd)
{
  HRESULT res = CoInitialize(NULL);
  if (FAILED(res))
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function CoInitialize. Error code: " << res);
  }
  BOOST_SCOPE_EXIT_ALL(=)
  {
    CoUninitialize();
  };

  // Explicitly load MSAA so we know if it's installed
  HINSTANCE oleacc_dll_hinstance = LoadLibraryA("OLEACC.DLL");
  if (oleacc_dll_hinstance == NULL)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function LoadLibraryA. Error code: " << GetLastError());
  }
  BOOST_SCOPE_EXIT_ALL(oleacc_dll_hinstance)
  {
    FreeLibrary(oleacc_dll_hinstance);
  };

  UINT msg_id = RegisterWindowMessageA("WM_HTML_GETOBJECT");
  if (!msg_id)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function RegisterWindowMessageA. Error code: " << GetLastError());
  }

  LRESULT lRes = 0;
  if (!SendMessageTimeoutA(
    internet_explorer_server_hwnd
    , msg_id
    , NULL
    , NULL
    , SMTO_ABORTIFHUNG
    , 1000
    , (PDWORD)&lRes
  ))
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while using function SendMessageTimeoutA. Error code: " << GetLastError());
  }

  LPFNOBJECTFROMLRESULT object_from_result_f = (LPFNOBJECTFROMLRESULT)GetProcAddress(oleacc_dll_hinstance, "ObjectFromLresult");
  if (object_from_result_f == NULL)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason("An error occurred while using function GetProcAddress");
  }

  CComPtr<IHTMLDocument2> html_doc;
  res = (*object_from_result_f)(lRes, IID_IHTMLDocument2, 0, (void**)&html_doc);
  if (FAILED(res))
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while getting IHTMLDocument2. Error code: " << res);
  }

  IHTMLElement* html_body;
  res = html_doc->get_body(&html_body);
  if (FAILED(res))
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while getting BODY element. Error code: " << res);
  }

  // the parent of BODY is HTML
  IHTMLElement* html_element;
  res = html_body->get_parentElement(&html_element);
  if (FAILED(res))
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while getting HTML element. Error code: " << res);
  }

  BSTR html_element_text;
  html_element->get_outerHTML(&html_element_text);

  return html_element_text ? html_element_text : std::wstring();
}

/**
 * \brief Closes the specified window
 * \throw koicxx::win::exception on failure
 */
inline
void close_window(HWND hwnd)
{
  // CloseWindow just minimizes the specified window
  // Also we can't use DestroyWindow (access is denied)
  if (SendMessageA(hwnd, WM_CLOSE, NULL, NULL))
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason("An error occurred while using function SendMessageA");
  }
}

const std::size_t MAX_LENGTH_OF_LPCOMMANDLINE_ARGUMENT = 32768;

/**
 * \brief Starts the specified process with the specified arguments
 * \throw koicxx::win::exception on failure
 */
inline
PROCESS_INFORMATION start_process(
  const std::string& exe_path
  , const std::string& args = ""
)
{
  const std::string lp_commandline_arg = "\"" + exe_path + "\" " + args;

  const std::size_t lp_commandline_arg_size_with_terminating_null = lp_commandline_arg.size() + 1;
  // The maximum length of lpCommandLine argument is 32,768 characters, including the Unicode terminating null character
  if (lp_commandline_arg_size_with_terminating_null > MAX_LENGTH_OF_LPCOMMANDLINE_ARGUMENT)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "Exceed max. limit of lpCommandLine argument. Length: " << lp_commandline_arg_size_with_terminating_null);
  }

  std::unique_ptr<char[]> lp_commandline_ptr(new char[lp_commandline_arg_size_with_terminating_null]);
  std::strcpy(lp_commandline_ptr.get(), lp_commandline_arg.c_str());

  STARTUPINFOA si;
  ZeroMemory(&si, sizeof(si));
  PROCESS_INFORMATION pi;
  ZeroMemory(&pi, sizeof(pi));
  if (!CreateProcessA(
    exe_path.c_str(),
    lp_commandline_ptr.get(),
    NULL,
    NULL,
    FALSE,
    0,
    NULL,
    NULL,
    &si,
    &pi
  ))
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "an error occurred while using function CreateProcessA. Error code: " << GetLastError());
  }

  return pi;
}

/**
 * \brief Terminates the specified process with the specified return code and waits for its termination
 * \throw koicxx::win::exception on failure
 */
inline
void terminate_process(HANDLE process_handle, UINT ret_code)
{
  if (!TerminateProcess(process_handle, ret_code))
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "an error occurred while using function TerminateProcess. Error code: " << GetLastError());
  }

  WaitForSingleObject(process_handle, INFINITE);
}

/**
 * \brief Waits for the specified process completion
 * \return Exit code of the specified process
 * \throw koicxx::win::exception on failure
 */
inline
DWORD wait_for_process_completion(HANDLE process_handle)
{
  WaitForSingleObject(process_handle, INFINITE);
  DWORD exit_code;
  if (!GetExitCodeProcess(process_handle, &exit_code))
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "an error occurred while using function GetExitCodeProcess. Error code: " << GetLastError());
  }
  return exit_code;
}

} // namespace win
} // namespace koicxx

#endif // !KOICXX_WIN_HPP